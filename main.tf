# Provider configuration for AWS
provider "aws" {
  region     = "us-east-2a"
  access_key = "AKIAYS2NSRPEK5D2KYM3"
  secret_key = "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"
}

# Provider configuration for AWS
provider "aws" {
  region = "us-east-2a"
}

# VPC Configuration
resource "aws_vpc" "fileupload" {
  cidr_block = "10.0.0.0/16"
  enable_dns_support = true
  enable_dns_hostnames = true
}

# Subnet Configuration
resource "aws_subnet" "subnet" {
  vpc_id                  = aws_vpc.fileupload.id
  cidr_block              = "10.0.1.0/24"
  availability_zone       = "your_az"   # Specify the availability zone for your subnet
}

# Security Group Configuration
resource "aws_security_group" "database_sg" {
  name        = "database-sg"
  description = "Security group for RDS instance"

  vpc_id = aws_vpc.fileupload.id

  // Inbound rule for MySQL
  ingress {
    from_port   = 3306
    to_port     = 3306
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]  # Allow access from anywhere (modify as needed)
  }

  // Outbound rule (example)
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

# Provisioning an Amazon RDS instance for the database
resource "aws_db_instance" "database_instance" {
  identifier            = "your_database_identifier"
  instance_class       = "db.t2.micro"
  engine               = "mysql"
  username             = "db_username"
  password             = "db_password"
  allocated_storage    = 20
  publicly_accessible  = false
  db_subnet_group_name = "default"
  vpc_security_group_ids = [aws_security_group.database_sg.id]
}

# Provisioning an Amazon S3 bucket for file storage
resource "aws_s3_bucket" "file_storage_bucket" {
  bucket = "fileuploadbucket"
}

# Provisioning an Amazon EKS cluster
resource "aws_eks_cluster" "kubernetes_cluster" {
  name     = "your_cluster_name"
  role_arn = "your_eks_service_role_arn"
  version  = "1.21"

  vpc_config {
    subnet_ids = [aws_subnet.subnet.id]
    security_group_ids = [aws_security_group.database_sg.id]
  }
}

# Output variables
output "s3_bucket_name" {
  value = aws_s3_bucket.file_storage_bucket.bucket
}

output "rds_endpoint" {
  value = aws_db_instance.database_instance.endpoint
}

output "eks_cluster_endpoint" {
  value = aws_eks_cluster.kubernetes_cluster.endpoint
}
