import os
import requests
import boto3
from botocore.exceptions import ClientError
from flask import Flask, request, render_template, jsonify

app = Flask(__name__)

# AWS credentials
AWS_ACCESS_KEY_ID = 'AKIAYS2NSRPEFGYFVVWU'
AWS_SECRET_ACCESS_KEY = '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'

# S3 bucket name
AWS_S3_BUCKET_NAME = 'fileuploadbuc'

# VirusTotal API key
VIRUSTOTAL_API_KEY = '5b54a393a401caaa9cbc3799450459397c264fcde6935896a3ef522ce81f3c2b'
VIRUSTOTAL_SCAN_URL = 'https://www.virustotal.com/vtapi/v2/file/scan'

# Configure S3 client
s3 = boto3.client('s3', aws_access_key_id=AWS_ACCESS_KEY_ID, aws_secret_access_key=AWS_SECRET_ACCESS_KEY)

@app.route('/')
def index():
    return render_template('index.html')

def scan_file(file_path):
    params = {'apikey': VIRUSTOTAL_API_KEY}
    files = {'file': (os.path.basename(file_path), open(file_path, 'rb'))}
    response = requests.post(VIRUSTOTAL_SCAN_URL, files=files, params=params)

    if response.status_code == 200:
        scan_result = response.json()
        if 'positives' in scan_result and scan_result['positives'] == 0:
            return True
    return False

@app.route('/upload', methods=['POST'])
def upload_file():
    # Check if the POST request has a file part
    if 'file' not in request.files:
        return 'No file part in the request', 400

    file = request.files['file']

    # If user does not select file, browser also
    # submit an empty part without filename
    if file.filename == '':
        return 'No selected file', 400

    file_path = os.path.join('C:\\Users\\Sheruby mondero\\Downloads\\New folder', file.filename)
    file.save(file_path)

    # Scan the file for viruses
    if scan_file(file_path):
        os.remove(file_path)
        return jsonify({'error': 'The uploaded file is infected with a virus and has been deleted'}), 400
    else:
        # Upload the file to S3
        try:
            s3.upload_file(file_path, AWS_S3_BUCKET_NAME, file.filename)
            os.remove(file_path)
            return jsonify({'message': f'File uploaded successfully to S3 bucket: {AWS_S3_BUCKET_NAME}/{file.filename}', 'virus_scan_result': 'Clean'}), 200
        except ClientError as e:
            os.remove(file_path)
            return jsonify({'error': f'Failed to upload file to S3: {e}'}), 500

if __name__ == '__main__':
    app.run(debug=True)
